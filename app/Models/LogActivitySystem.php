<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogActivitySystem extends Model{
    protected $table = "log_activity_system";
    protected $primaryKey = 'log_activity_system_id';
    protected $guarded = ['log_activity_system_id'];
}