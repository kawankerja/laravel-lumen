<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Game;
use App\Models\Photo;
use DB;
use Carbon;
use Illuminate\Support\Str;

class LogActivity extends Model{

    protected $table = "log_activity";
    protected $primaryKey = 'log_activity_id';
    protected $guarded = ['log_activity_id'];

    const LOGIN = 1;
    const LOGOUT = 2;  
    const PLAY = 3; 
    const PHOTO = 4; 
    const POINT = 5; 
    // const REWARD_REQUESTED  = 6; 
    // const REWARD_PURCHASED  = 7; 
    const REDEEM  = 6; 
    const QUESTIONARE  = 7; 
    
    public static function getDescription($type,$id = NULL,$data = NULL){
     
        $result = '';
        $dateFormat = "Y-m-d H:i:s";
        switch ($type) {
            case self::LOGIN:
                $result = "Has Login in ".date($dateFormat);
                break;
            case self::LOGOUT:
                $result = "Has Logout in ".date($dateFormat);
                break;
            case self::PLAY:
                $result = "Play $data->name in ".date($dateFormat);
                break;
            case self::PHOTO:
                $result = "Upload photo $data in ".date($dateFormat);
                break;
            case self::POINT:
                $result = "Get ".$data['total'] ." point from ".$data['source']." in ".date($dateFormat);
                break;
            // case self::REWARD_REQUESTED:
            //     $dataReward = Reward::find($id);
            //     $result = "Requested a $dataReward->name item reward on ".date($dateFormat);
            //     break;
            // case self::REWARD_PURCHASED:
            //     $dataReward = Reward::find($id);
            //     $result = "Redeem a $dataReward->name item reward on ".date($dateFormat);
            //     break;
            case self::REDEEM:
                $result = "Redeem \" ".$data['comment']." \" an item reward on ".date($dateFormat);
                break;
            case self::QUESTIONARE:
                $result = "Fill a questionare on ".date($dateFormat);
                break;
            default:
            break;
            
        }

        return $result;
    }

    public static function isGetPointFromVideoToday($player,$keyword){
        $today = Carbon\Carbon::now();
        $result = DB::select("SELECT * FROM log_activity WHERE DATE(created_at) = '".$today->toDateString()."' AND meta_data LIKE '%$keyword%' AND player_id = $player");
        return count($result) > 0;
    }

    public static function GetPointToday($playerId){
        $today = Carbon\Carbon::now();
        $result = DB::select("SELECT IFNULL(SUM(json_extract(meta_data, '$.total')),0)  as point FROM log_activity la WHERE player_id = $playerId AND activity_type_id = ".self::POINT." AND DATE(created_at) = '".$today->toDateString()."' ");

        return $result[0]->point;

    }

    public static function getPointGamesToday($playerId){
        $today = Carbon\Carbon::now();
        $result = DB::select("SELECT 
        IFNULL(SUM(json_extract(meta_data, '$.total')),0) as point ,
        json_extract(meta_data, '$.source') as game
        FROM log_activity la WHERE player_id = 
        $playerId AND activity_type_id = ".self::POINT." AND DATE(created_at) 
        = '".$today->toDateString()."' 
        AND json_extract(meta_data, '$.source') LIKE '%Game%'
        GROUP BY json_extract(meta_data, '$.source')");
      
        return $result;

    }

    public static function isFollowSosmed($playerId){
        $today = Carbon\Carbon::now();
        $result = DB::select("SELECT 
         *
        FROM log_activity la WHERE player_id = 
        $playerId AND activity_type_id = ".self::POINT." AND DATE(created_at) 
        = '".$today->toDateString()."' 
        AND json_extract(meta_data, '$.source') LIKE '%sosmed%'");

        return count($result) > 0;
    }

}