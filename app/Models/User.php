<?php   

namespace App\Models;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Models\Player;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class User extends Model implements JWTSubject,AuthenticatableContract, CanResetPasswordContract{
    use Authenticatable, CanResetPassword;

    protected $table = 'user';
    protected $primaryKey = 'user_id';
    protected $guarded = ['user_id'];

    public function player(){
        return $this->hasOne(Player::class,'user_id');
    }

    public function roles(){
        return $this->belongsToMany(Role::class,'user_role','user_id','role_id')->withPivot('user_role_id');
    }
    
    public static function isAdmin($id){
        $result = false;
        $roles = self::find($id)->roles;
        foreach ($roles as $role) {
           if ($role->role_id == UserRole::SUPER_ADMIN){
               $result = true;
               break;
           }
        }
        return $result;
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}