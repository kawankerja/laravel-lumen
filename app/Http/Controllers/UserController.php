<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\LogActivity;
use App\Models\LogActivitySystem;
use App\Models\Player;
use App\Models\HistoryTotal;
use Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller{

    public function __construct()
    {
        parent::__construct(new User(),"user");
    }

    public function index(Request $request){
        
        $message = "List ".$this->entity;

        $userRoleModel = new UserRole();
        $userRoleId = $userRoleModel->getKeyName();
        $userRoleTable = $userRoleModel->getTable();

        $roleModel = new Role();
        $roleId = $roleModel->getKeyName();
        $roleTable = $roleModel->getTable();

        $result = DB::table($this->entity)
        ->join($userRoleTable, $userRoleTable.'.'.$this->entityId, '=', $this->entity.'.'.$this->entityId)
        ->join($roleTable, $roleTable.'.'.$roleId, '=', $userRoleTable.'.'.$roleId)
        ->select($this->entity.'.*',$roleTable.'.name as role')
        ->whereIn($userRoleTable.'.'.$roleId, [Role::SUPER_ADMIN, Role::SUPER_ADMIN, Role::USER])
        ->orderBy($this->entity.'.created_at','DESC')
        ->get();

        $response = [
            "success" => true,
            "message" => $message,
            "total" => count($result),
            "data" => $result
        ];
       LogActivitySystem::create([
            'type' => 'read',
            'tables' => $this->entity,
            'is_success' => 1,
            'user_id' => auth()->user()->user_id,
            // 'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response);
    }

    public function show($id){
        $message = "Data ".$this->entity;
        $isSuccess = true;
        $code = 200;

        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
        }else{
            $result['roles'] = $result->roles;
           
        }

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'read',
            'params' => json_encode([$this->entityId => $id]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response)
        ]);

        return response()->json($response,$code);
    }
    
    public function createWithFile(Request $request){

        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'role_ids' => 'required|array',
            'email' => 'required|email|unique:user,email'
        ]);

        if ($validator->fails()) {
            return  response()->json([
                'success' => false,
                'message' => $validator->errors()->all(),
                'data' => $validator->errors()
            ],422);
        }

        $message = "Data $this->entity has been created";
        $isSuccess = true;
        $code = 201;
        
        $roleModel = new Role();
        $dataPost = $request->input();

        $roleIds = $dataPost[$roleModel->getKeyName().'s' ];

        $dataPost['password'] = Hash::make($request->input('password'));
        $dataPost['created_by'] = auth()->user()->email;
        unset($dataPost[$roleModel->getKeyName().'s' ]);
        $dataFile = $request->file();

        if (count($dataFile) > 0){

            $imageKey = array_key_first($dataFile);
            $originalFilename = $dataFile[$imageKey]->getClientOriginalName();
            $originalFilenameArr = explode('.', $originalFilename);
            $fileExt = end($originalFilenameArr);
            $imageFileName = 'U-' . time() . '.' . $fileExt;
            $dataPost[$imageKey] = $imageFileName;

            $result = $this->model->create($dataPost);
            $path = $this->getDefaultUploadPath($result->{$this->entityId});
          

            if ($dataFile[$imageKey]->move($path, $imageFileName)) {
                $result->path = $path.$imageFileName;
            } else {
                $message = "Cannot upload image $this->entity ";
                $isSuccess = false;
                $this->model->where($this->entityId,$result->{$this->entityId})->delete();

            }

        }else{
            $result = $this->model->create($dataPost);
        }

       
        $userRoleModel = new UserRole();
        
        foreach ($roleIds as $roleId) {
            $userRoleModel->create([
                $this->entityId => $result->{$this->entityId},
                $roleModel->getKeyName() => $roleId
            ]);
        }

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'create',
            'body' => json_encode($request->input()),
            'tables' => json_encode(['user','role','user_role']),
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response)
        ]);
        return response()->json($response,$code);

    }

    public function updateWithFile(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'email' => 'email|unique:user,email',
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return  response()->json([
                'success' => false,
                'message' => $validator->errors()->all(),
                'data' => $validator->errors()
            ],422);
        }

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        $dataPost = $request->input();

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            $result = $request->input();
            goto result;
        }
        
        $dataPost['updated_by'] = auth()->user()->email;

        if (array_key_exists('roles',$dataPost)){
            $roleModel = new Role();
            $userRoleModel = new UserRole();
            $userRoleKey = $userRoleModel->getKeyName();
            $roles = [];

            //change format so can update batch 
            foreach ($dataPost['roles'] as $role) {
                $roles[$role[$userRoleKey]] = [];
                foreach ($role as $key => $value) {
                    $roles[$role[$userRoleKey]][$key]= $value; 
                }
                unset( $roles[$role[$userRoleKey]][$userRoleKey]);
            }

            $this->updateBatchQuery($roles,$userRoleModel->getTable(),$userRoleKey);
            unset($dataPost['roles']);
        }

        if (array_key_exists('password',$dataPost)){
            $dataPost['password'] = Hash::make($request->input('password'));
            unset($dataPost['password_confirmation']);
        }

        $dataFile = $request->file();
        if (count($dataFile) > 0){

            $imageKey = array_key_first($dataFile);
            $originalFilename = $dataFile[$imageKey]->getClientOriginalName();
            $originalFilenameArr = explode('.', $originalFilename);
            $fileExt = end($originalFilenameArr);
            $path = $this->getDefaultUploadPath($id);
            $imageFileName = 'U-' . time() . '.' . $fileExt;

            if ($dataFile[$imageKey]->move($path, $imageFileName)) {
                $dataPost[$imageKey] = $imageFileName;
            } else {
                $message = "Cannot upload image $this->entity ";
                $isSuccess = false;
                goto result;
            }

        }

        $this->model->where($this->entityId,$id)->update($dataPost);

        result:
        $response =[
            "success" => $isSuccess,
            "message" => $message,
            "data" => $dataPost
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'params' => json_encode([$this->entityId => $id]),
            'body' => json_encode($request->input()),
            'tables' => json_encode(['user','role','user_role']),
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response)
        ]);
        return response()->json($response,$code);
    }

    public function delete($id){

        $message = "Data $this->entity has been deleted";
        $isSuccess = true;
        $code = 200;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            goto result;
        }

        //delete user role
        $id = DB::select("SELECT GROUP_CONCAT(user_role_id) as ids from user_role WHERE user_id = 2")[0]->ids;
        $ids = explode(",", $id);
        UserRole::destroy($ids);
       
        $result = $this->model->where($this->entityId,$id)->delete();

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'delete',
            'params' => json_encode([$this->entityId => $id]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response,$code);
    }

    public function logoutInactivityUser(){

        
        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        $logActivityModel = new LogActivity();
        $logActivityTable = $logActivityModel->getTable();
        $logActivityKey = $logActivityModel->getKeyName();
        $playerModel = new Player();
        $playerTable = $playerModel->getTable();
        $playerKey = $playerModel->getKeyName();

        $now = Carbon::now()->toDateTimeString();

        $userIds = DB::select('select user.user_id from `user` inner join `player` on `player`.`user_id` = `user`.`user_id` inner join `log_activity`
         on `log_activity`.`player_id` = `player`.`player_id` where TIMESTAMPDIFF(MINUTE,log_activity.created_at,NOW())
          > 30 and user.status_login = 1 GROUP BY user.user_id');

        if (!$userIds){
            $isSuccess = false;
            $message = "No inactive user";
        }else{
            $users = [];
            foreach ($userIds as $user) {
                $users[$user->{$this->entityId}] = [];
                $users[$user->{$this->entityId}]['status_login'] = 0; 
            }
     
            $this->updateBatchQuery($users,$this->entity,$this->entityId);
            $this->updateDataHistory(HistoryTotal::TOTAL_VISITOR_ONLINE);
        }
       
        result:
        return response()->json([
            "success" => $isSuccess,
            "message" => $message,
            "data" => $userIds
        ],$code);
    }

    function updateDataHistory($type){


        $dataHistoryTotal = HistoryTotal::where('record_time',date('Y-m-d'))
        ->where('type',$type)->get();

        $total =  $type == HistoryTotal::TOTAL_VISITOR ? Player::all()->count() :  Role::getTotalLogin();

        if (count($dataHistoryTotal) > 0){

            $updateArr = [];
            $updateArr['total'] = $total;
            HistoryTotal::where('record_time',date('Y-m-d'))
            ->where('type',$type)->update($updateArr);

        }else{
            $updateArr = [];
            $updateArr['record_time'] = date('Y-m-d');
            $updateArr['type'] = $type;
            $updateArr['total'] =  $total;
            HistoryTotal::create($updateArr);

        }
    }


}