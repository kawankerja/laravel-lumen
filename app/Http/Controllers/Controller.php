<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\LogActivitySystem;
use DB;

class Controller extends BaseController
{
    //
    protected $model;
    protected $entity;
    protected $entityId;

    public function __construct($model,$entity){
        $this->model = $model;
        $this->entity = $entity;
        $this->entityId = 'id';
    }

    public function index(Request $request){
        
        $message = "List ".$this->entity;
        $offset = $request->query('offset') ? $request->query('offset') : 0;
        $limit = $request->query('limit') ? $request->query('limit') : 6;

        $result = $this->model->offset($offset)->limit($limit)->orderBy('created_at','DESC')->get(); ;
        $response = [
            "success" => true,
            "message" => $message,
            "total" => count($result),
            "data" => $result
        ];

        $userId = NULL;
        if (auth()->user()){
            $userId = auth()->user()->user_id;
        } 

       LogActivitySystem::create([
            'type' => 'read',
            'tables' => $this->entity,
            'user_id' => $userId,
            'is_success' => 1,
            'response' => json_encode($response)
        ]);
        return response()->json($response);
    }

    public function show($id){
        $message = "Data ".$this->entity;
        $isSuccess = true;
        $code = 200;

        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
        }

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        $userId = NULL;
        if (auth()->user()){
            $userId = auth()->user()->user_id;
        }

        LogActivitySystem::create([
            'type' => 'read',
            'params' => json_encode([$this->entityId => $id]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => $userId,
            'code' => $code,
            'response' => json_encode($response)
        ]);

        return response()->json($response,$code);
    }

    public function allBy(Request $request,$columnName,$value){

        $message = "List ".$this->entity;
        $offset = $request->query('offset') ? $request->query('offset') : 0;
        $limit = $request->query('limit') ? $request->query('limit') : 6;
        
        $result = $this->model->where($columnName,$value)->offset($offset)->limit($limit)->orderBy('created_at','DESC')->get();
        
        $response = [
            "success" => true,
            "message" => $message,
            "total" => count($result),
            "data" => $result
        ];

        $userId = NULL;
        if (auth()->user()){
            $userId = auth()->user()->user_id;
        }

        LogActivitySystem::create([
            'type' => 'read',
            'params' => json_encode([$columnName => $value]),
            'tables' => $this->entity,
            'is_success' => 1,
            'user_id' => $userId,
            'response' => json_encode($response)
        ]);

        return response()->json($response);
    }

    public function showBy($columnName,$value){
        $message = "Data ".$this->entity;
        $isSuccess = true;
        $code = 200;

        $result = $this->model->where($columnName,$value)->first();
 
        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
        }

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        $userId = NULL;
        if (auth()->user()){
            $userId = auth()->user()->user_id;
        }

        LogActivitySystem::create([
            'type' => 'read',
            'params' => json_encode([$columnName => $value]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => $userId,
            'code' => $code,
            'response' => json_encode($response)
        ]);

        return response()->json($response,$code);
    }

    public function count(Request $request){
        $message = "Data count ".$this->entity;
        $isSuccess = true;
        $code = 200;
        $data = $request->query();
        $firstKey = array_keys($data)[0];
        $firstValue = $data[$firstKey];
     
        $builder = $this->model->where($firstKey,$firstValue);
        unset($data[$firstKey]);
        foreach ($data as $key => $value) {
           $builder->where($key,$value);
        }
        $result = ['total' => $builder->count()];

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'read',
            'tables' => $this->entity,
            'query' => $data,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);
    }

    public function collectCountBar($columnName){
        $message = "Data count ".$this->entity;
        $isSuccess = true;
        $code = 200;
        
        $data = $this->model->orderBy($columnName,'DESC')->get();
        $result = [];

        $max = 5;
        $i=0;
        $count = count($data);

        while($i < $max && $i < $count){
            
            array_push($result,[
                'name' => $data[$i]->name,
                'y' => $data[$i]->{$columnName},
                'drilldown' => $data[$i]->name
            ]);
            $i++;
        }

        if ($count > $max){
            $totalOther = 0;
            for ($i=$max; $i < $count; $i++) { 
                $totalOther += $data[$i]->{$columnName};
            }
            array_push($result,[
                'name' => 'other',
                'total' => $totalOther,
                'drilldown' => null
            ]);
        }

        $dataResult = [
            'name' => $this->entity.'s',
            'colorByPoint' => true,
            'data' => $result
        ];

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'read',
            'params' => json_encode([$columnName]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);
    }

    public function create(Request $request){

        $message = "Data $this->entity has been created";
        $isSuccess = true;
        $code = 201;
        
        $dataPost = $request->input();
        $dataPost['created_by'] = auth()->user()->email;

        $result = $this->model->create($dataPost);

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'create',
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);

    }

    public function createWithFile(Request $request){

        $message = "Data $this->entity has been created";
        $isSuccess = true;
        $code = 201;
        
        $dataPost = $request->input();
        $dataFile = $request->file();
        $dataPost['created_by'] = auth()->user()->email;

        if (count($dataFile) > 0){

            $imageKey = array_key_first($dataFile);
            $originalFilename = $dataFile[$imageKey]->getClientOriginalName();
            $originalFilenameArr = explode('.', $originalFilename);
            $fileExt = end($originalFilenameArr);
            $imageFileName = 'U-' . time() . '.' . $fileExt;
            $dataPost[$imageKey] = $imageFileName;

            $result = $this->model->create($dataPost);
            $path = $this->getDefaultUploadPath($result->{$this->entityId});
          

            if ($dataFile[$imageKey]->move($path, $imageFileName)) {
                $result->path = $path.$imageFileName;
            } else {
                $message = "Cannot upload image $this->entity ";
                $isSuccess = false;
                $this->model->where($this->entityId,$result->{$this->entityId})->delete();

            }

        }else{

            $result = $this->model->create($dataPost);
        }

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'create',
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);

    }

    public function createBatch(Request $request){

        $message = "Data $this->entity has been created";
        $isSuccess = true;
        $code = 201;

        
        $dataPost = $request->input();
        $dataPost = $dataPost["data"];
        
        foreach ($dataPost as $key => $value) {
            $dataPost[$key]['created_by'] = auth()->user()->email;
        }

        $result = $this->model->insert($dataPost);

        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'create',
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);

    }

    public function update(Request $request,$id){

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            $result = $request->input();
            goto result;
        }
        $dataPost = $request->input();
        $dataPost['updated_by'] = auth()->user()->email;
        $this->model->where($this->entityId,$id)->update($dataPost);

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $dataPost
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'params' => json_encode([$this->entityId => $id]),
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        
        return response()->json($response,$code);
    }

    public function updateWithFile(Request $request,$id){

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        $dataFile = $request->file();
        $dataPost = $request->input();
        $dataPost['updated_by'] = auth()->user()->email;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            $result = $request->input();
            goto result;
        }

        if (count($dataFile) > 0){

            $imageKey = array_key_first($dataFile);
            $originalFilename = $dataFile[$imageKey]->getClientOriginalName();
            $originalFilenameArr = explode('.', $originalFilename);
            $fileExt = end($originalFilenameArr);
            $path = $this->getDefaultUploadPath($id);
            $imageFileName = 'U-' . time() . '.' . $fileExt;

            if ($dataFile[$imageKey]->move($path, $imageFileName)) {
                $dataPost[$imageKey] = $imageFileName;
            } else {
                $message = "Cannot upload image $this->entity ";
                $isSuccess = false;
                goto result;
            }

        }

        $this->model->where($this->entityId,$id)->update($dataPost);

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $dataPost
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'params' => json_encode([$this->entityId => $id]),
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response,$code);
    }

    public function updateBatch(Request $request){

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        $dataPost = $request->input();
        $dataPost['updated_by'] = auth()->user()->email;

        if (array_keys($dataPost)[0] == 0){
            array_unshift($dataPost,"");
            unset($dataPost[0]);
        }
        $this->updateBatchQuery($dataPost,$this->entity,$this->entityId);
       
        $result = $dataPost;
        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);
    }

    public function updateAll(Request $request){

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        $dataPost = $request->input();
        $dataPost['updated_by'] = auth()->user()->email;

        $this->model->query()->update($dataPost);
       
        $result = $dataPost;
        
        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);

        return response()->json($response,$code);
    }

    public function increment(Request $request,$id,$columnName){

        $message = "Data $this->entity has been updated";
        $isSuccess = true;
        $code = 200;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            goto result;
        }
        if (!is_numeric($result[$columnName])){
            $isSuccess = false;
            $message = "Column is not numeric";
            $code = 422;
            goto result;
        }
   
        $this->model->where($this->entityId,$id)->increment($columnName);
        $result[$columnName] = intval($result[$columnName])+1;

        $this->model->where($this->entityId,$id)->update(['updated_by' => auth()->user()->email]);

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'params' => json_encode([$columnName => $id]),
            'body' => json_encode($request->input()),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response,$code);
    }

    public function delete($id){

        $message = "Data $this->entity has been deleted";
        $isSuccess = true;
        $code = 200;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            goto result;
        }

       
        $result = $this->model->where($this->entityId,$id)->delete();

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'delete',
            'params' => json_encode([$this->entityId => $id]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response,$code);
    }

    public function activate($id,$value){

        $message = $value == 1 ? //1 or 0
        "Data $this->entity has been activated" : 
        "Data $this->entity has been deactivated";
        $isSuccess = true;
        $code = 200;

        //not found
        $result = $this->model->find($id);

        if (!$result){
            $isSuccess = false;
            $message = "Data with id = ".$id." not found";
            $code = 404;
            goto result;
        }

        $this->model->where($this->entityId,$id)->update(["status"=>$value,"updated_by"=>auth()->user()->email]);

        result:
        $response = [
            "success" => $isSuccess,
            "message" => $message,
            "data" => $result
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'params' => json_encode([$id => $value]),
            'tables' => $this->entity,
            'is_success' => $isSuccess,
            'user_id' => auth()->user()->user_id,
            'code' => $code,
            'response' => json_encode($response),
            'created_by' => auth()->user()->email
        ]);
        return response()->json($response,$code);
    }

    public function getDefaultUploadPath($id){
        return "./upload/$this->entity/$id/";
    }

    public function decodeBase64Image($id,$imageFileName){
        $path = $this->getDefaultUploadPath($id).'/'.$imageFileName;
        $result = '';
        if (file_exists($path)){
            $data = file_get_contents($path);
            $result = base64_encode($data);
        }
        

        return $result;
    }

    public function updateBatchQuery($dataPost,$tableName,$tableKey){
        $ids = [];
        $params = [];
        $columnsGroups = [];
        $queryStart = "UPDATE {$tableName} SET";
        $columnsNames = array_keys(array_values($dataPost)[0]);
        foreach ($columnsNames as $columnName) {
            $cases = [];
            $columnGroup = " ".$columnName ." = CASE $tableKey ";
            foreach ($dataPost as $id => $newData){
                $cases[] = "WHEN {$id} then ?";
                $params[] = $newData[$columnName];
                $ids[$id] = "0";
            }
            $cases = implode(' ', $cases);
            $columnsGroups[] = $columnGroup. "{$cases} END";
        }
        $ids = implode(',', array_keys($ids));
        $columnsGroups = implode(',', $columnsGroups);
        // $params[] = Carbon::now();
        $queryEnd = " WHERE $tableKey in ({$ids})";
  
        DB::update($queryStart.$columnsGroups. $queryEnd, $params);
    }

}
