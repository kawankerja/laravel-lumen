<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Player;
use App\Models\LogActivity;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\Game;
use App\Models\PlayerGame;
use App\Models\HistoryTotal;
use App\Models\LogActivitySystem;
use Validator;
use DB;
use App\Helper\Helper;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:user',
            'password' => 'required|confirmed'
            // 'phone_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return  response()->json([
                'success' => false,
                'message' => $validator->errors()->all(),
                'data' => $validator->errors()
            ],422);
        }

        $username = $request->input('username');
        // $phoneNumber = $request->input('phone_number');
        $password = Hash::make($request->input('password'));
   
        $user = User::create([
            'username' => $username,
            'password' => $password,
            // 'phone_number' => $phoneNumber,
            'created_by' => $username
        ]);

        $response = [
            'success' => true,
            'message' => 'Registration Successfull',
            'data' => null
        ];

        LogActivitySystem::create([
            'type' => 'create',
            'body' => json_encode($request->all()),
            'tables' => json_encode(['user']),
            'response' => json_encode($response),
            'created_by' => $username
        ]);

        return response()->json($response,200);

       
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {   
        
       
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return  response()->json([
                'success' => false,
                'message' => $validator->errors()->all(),
                'data' => $validator->errors()
            ],422);
        }

        $credentials = $request->only(['username', 'password']);
        $TTL = 1440; //default 1 day


        if (! $token = auth()->setTTL($TTL)->attempt($credentials)) {
            $message = 'Unauthorized';

            $userData = User::where('username', $request->input('username'))->first();
            if (!$userData){
                $message = "Username is not registered";
            }else{
                $message = "Wrong password";
            }

            return response([
                'success' => false ,
                'message' => $message,
                'data' => null
            ], 401);
        }

        User::where('username', $request->input('username'))
        ->update([
            'remember_token' => $token,
            'status_login' => true,
            'updated_by' => $request->input('username')
        ]);
        
        
        result:

    //    $user =  auth()->user();
    //    $user['roles'] = User::find($user->user_id)->roles;

       $response = [
            'success' => true,
            'message' => 'Login successfull',
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ]
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'body' => json_encode($request->input()),
            'tables' => json_encode(['user','player','role','log_activity']),
            'response' => json_encode($response)
        ]);

       return response()->json($response);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user =  auth()->user();

        $response = [
            'success' => true,
            'message' => 'User data',
            'data' => auth()->user()
        ];

       LogActivitySystem::create([
            'type' => 'read',
            'tables' => json_encode(['user','player','role']),
            'is_success' => 1,
            'user_id' => auth()->user()->user_id,
            'response' => json_encode($response)
        ]);

        return response()->json($response);
    }
    
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        User::where('username', auth()->user()->username)
        ->update([
            'status_login' => false,
            'updated_by' => auth()->user()->username
        ]);

        LogActivity::create([
            'activity_type_id' => LogActivity::LOGOUT,
            'description' => LogActivity::getDescription(LogActivity::LOGOUT),
            'created_by' => auth()->user()->username
        ]);

        // $this->updateDataHistory(HistoryTotal::TOTAL_VISITOR_ONLINE);

        auth()->logout();

        $response = [
            'success' => true,
            'message' => 'Successfully logged out',
            'data' => null
        ];

        LogActivitySystem::create([
            'type' => 'update',
            'tables' => json_encode(['user']),
            'response' => json_encode($response)
        ]);

        return response()->json($response);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {   

        return response()->json([
            'success' => true,
            'message' => 'Login successfull',
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ]
        ]);
    }

}