<?php 
namespace App\Helper;

class Helper {

    function updateDataHistory($type){


        $dataHistoryTotal = HistoryTotal::where('record_time',date('Y-m-d'))
        ->where('type',$type)->get();

        $total =  $type == HistoryTotal::TOTAL_VISITOR ? Player::all()->count() :  Role::getTotalLogin();

        if (count($dataHistoryTotal) > 0){

            $updateArr = [];
            $updateArr['total'] = $total;
            HistoryTotal::where('record_time',date('Y-m-d'))
            ->where('type',$type)->update($updateArr);

        }else{
            $updateArr = [];
            $updateArr['record_time'] = date('Y-m-d');
            $updateArr['type'] = $type;
            $updateArr['total'] =  $total;
            HistoryTotal::create($updateArr);

        }
    }
}
?>