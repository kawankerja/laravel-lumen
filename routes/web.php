<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$version = "v1";

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//auth
$router->group(['prefix' => "api/$version/auth"], function () use ($router) {
    $router->post('register','AuthController@register');
    $router->post('login','AuthController@login');
});

$router->group(['prefix' => "api/$version/auth", 'middleware' => 'auth'], function () use ($router) {
    $router->get('me','AuthController@me');
    $router->get('refresh','AuthController@refresh');
    $router->post('logout','AuthController@logout');
});


//major crud wajib login
generateRouteAuth($router,'user','UserController');
generateRouteAuth($router,'video','VideoController');
generateRouteAuth($router,'button','ButtonController');
generateRouteAuth($router,'video-button','VideoButtonController');
generateRouteAuth($router,'link','LinkController');



//additional

//Video Button
$router->group(['prefix' => 'api/v1/video-button'], function () use ($router) {
    $router->get('','VideoButtonController@index');
});


function generateRouteAuth($router,$entity, $controller){

    $router->group(['prefix' => 'api/v1/'.$entity, 'middleware' => 'auth'], function () use ($router,$controller) {
        $router->get('',$controller.'@index');
        $router->get('/{id:[0-9]+}',$controller.'@show');
        $router->get('/all-by/{columnName:[A-Za-z_-]+}/{value}',$controller.'@allBy');
        $router->get('/show-by/{columnName:[A-Za-z_-]+}/{value}',$controller.'@showBy');
        $router->get('/count',$controller.'@count');
        $router->get('/collect-count-bar/{columnName:[A-Za-z_-]+}',$controller.'@collectCountBar');
        $router->post('',$controller.'@create');
        $router->post('/batch',$controller.'@createBatch');
        $router->put('/{id:[0-9]+}',$controller.'@update');
        $router->put('',$controller.'@updateBatch');
        $router->put('/increment/{columnName:[A-Za-z_-]+}/{id:[0-9]+}',$controller.'@increment');
        $router->delete('/{id:[0-9]+}',$controller.'@delete');
        $router->put('/activate/{id:[0-9]+}/{value}',$controller.'@activate');
    });

}

function generateRoute($router,$entity, $controller){

    $router->group(['prefix' => 'api/v1/'.$entity], function () use ($router,$controller) {
        $router->get('',$controller.'@index');
        $router->get('/{id:[0-9]+}',$controller.'@show');
        $router->get('/all-by/{columnName:[A-Za-z_-]+}/{value}',$controller.'@allBy');
        $router->get('/show-by/{columnName:[A-Za-z_-]+}/{value}',$controller.'@showBy');
        $router->get('/count',$controller.'@count');
        $router->get('/collect-count-bar/{columnName:[A-Za-z_-]+}',$controller.'@collectCountBar');
        $router->post('',$controller.'@create');
        $router->post('/batch',$controller.'@createBatch');
        $router->put('/{id:[0-9]+}',$controller.'@update');
        $router->put('',$controller.'@updateBatch');
        $router->put('/increment/{columnName:[A-Za-z_-]+}/{id:[0-9]+}',$controller.'@increment');
        $router->delete('/{id:[0-9]+}',$controller.'@delete');
        $router->put('/activate/{id:[0-9]+}/{value}',$controller.'@activate');
    });

}